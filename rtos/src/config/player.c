/*
 * main.c
 *
 *  Created on: 10-Sep-2017
 *      Author: aksonlyaks
 */

#include <stdio.h>
#include <string.h>
#include <player.h>
//#include <udi_cdc.h>

#define MODE_BYTES			2
#define TOP_INDX			35
#define BOTTOM_INDX			12
#define DATABYTES			20			//Length of data
#define	FIRST_CRC_LEN		1
#define SCND_CRC_LEN		2
#define FIRST_CRC_POS		DATABYTES - (SCND_CRC_LEN + FIRST_CRC_LEN)
#define SCND_CRC_POS		DATABYTES - (SCND_CRC_LEN)
//#define DEBUG
#define printf
char outBuffer[20];
int mRetailerMode = 1;
volatile unsigned char freq_to_send[DATABYTES * 2];
extern unsigned char aesKey[];
unsigned char* AESnative_c(char *state, unsigned char *key, unsigned char dir);


void CRC16native_c(char data[],int n, int compress) {
    int len=n;
    char bt;
    int wCrc = 0x6363;
    int idx = 0;

    do {
		bt = data[idx];
		idx++;

		bt = (char)(((bt & (0xFF)) ^ (wCrc & 0xFF)) & (0xFF));
		bt = (char)(((bt & (0xFF)) ^ ((bt << 4) & (0xFF))) & (0xFF));
		wCrc = (((wCrc) >> 8) & (0x00FFFFFF)) ^ ((bt & (0xFF)) << 8) ^ ((bt & (0xFF)) << 3) ^
			   (((bt & (0xFF)) >> 4) & (0x0FFFFFFF));
    } while (idx < len);

    if(compress)
    	data[n] = (char)(wCrc & 0xFF) + (char)((wCrc >> 8) & 0xFF);
    else
    {
    	data[n] = (char)(wCrc & 0xFF);
    	data[n+1] = (char)((wCrc >> 8) & 0xFF);
    }

    return;
}

void compressPlainStringNative(char s[], char* comp){

	int i, strLen = strlen(s), j =0;;
	char strmap, bit_rem = 6, byte_ind = 8;

	for(i = 0; i < strLen; i++)
	{
		if(s[i] >= 'A' && s[i] <= 'Z')
			strmap = s[i] - 'A' + 27;
		else if(s[i] >= 'a' && s[i] <= 'z')
			strmap = s[i] - 'a' + 1;
		else if(s[i] >= '0' && s[i] <= '9')
			strmap = s[i] - '0' + 53;
		else if(s[i] == ' ')
			strmap = 64;
here:
		if(byte_ind == 8)
		{
			comp[j++] |= (strmap << (8 - bit_rem));
			byte_ind = 8 - bit_rem;
			bit_rem = 0;
		}
		else
		{
			comp[j - 1] |= (strmap >> (6 - byte_ind));
			bit_rem = 6 - byte_ind;
			if(bit_rem == 0)
			{
				bit_rem = 6;
				byte_ind = 8;
				continue;
			}
			comp[j++] |= strmap << (8 - bit_rem);
			byte_ind = 8 - bit_rem;
		}
	}

	return;

}

void compressDigitStringNative(char s[], char* comp){

	int i, strLen = strlen(s), j =0;;
	char strmap, bit_rem = 4, byte_ind = 8;

	for(i = 0; i < strLen; i++)
	{
		if(s[i] >= 'A' && s[i] <= 'F')
			strmap = s[i] - 'A' + 11;
		else if(s[i] >= 'a' && s[i] <= 'z')
			strmap = s[i] - 'a' + 1;
		else if(s[i] >= '0' && s[i] <= '9')
			strmap = s[i] - '0' + 1;
		else if(s[i] == ' ')
			strmap = 64;
here:
		printf("strmap:%d s[i]:%c\n", strmap, s[i]);
		if(byte_ind == 8)
		{
			comp[j++] |= (strmap << (8 - bit_rem));
			byte_ind = 8 - bit_rem;
			bit_rem = 0;
		}
		else
		{
			comp[j - 1] |= (strmap >> (4 - byte_ind));
			bit_rem = 4 - byte_ind;
			if(bit_rem == 0)
			{
				bit_rem = 4;
				byte_ind = 8;
				continue;
			}
			comp[j++] |= strmap << (8 - bit_rem);
			byte_ind = 8 - bit_rem;
		}
	}

	return;
}

void ToneGenerator(char *toSendString)
{
    char *stringBytes;
	char quads = 0, byteQuad, i, count= 0;
	char fifo[TOP_INDX- BOTTOM_INDX], cur = 0;
	char freq;

	memset(outBuffer, 0, DATABYTES);
	memset(freq_to_send, 0, DATABYTES);
    stringBytes = &outBuffer[2];

    compressDigitStringNative(toSendString, stringBytes);
    //compressPlainStringNative(toSendString, stringBytes);
       //////////////////compress
#ifdef DEBUG
	printf("comresssedString stringBytes\n");
	int a;
	for(a=0;a<strlen(stringBytes);a++)
	{
			printf("%2x ",stringBytes[a]);
	}
	printf("\n");
#endif
	///////////////////end
    CRC16native_c(stringBytes,FIRST_CRC_POS - MODE_BYTES, 1);

#ifdef DEBUG
    printf("CRC\n");
    for(a=0;a<17;a++)
	{
			printf("%x ",stringBytes[a]);
	}
	printf("\n");
#endif

	AESnative_c(stringBytes,aesKey,0);

#ifdef DEBUG
	////////////////////print
	printf("AES encrypted cipher text\n");
	int aes;
	for (aes=0;aes<16;aes++) {
		printf(" %x ",stringBytes[aes]&0xff);
	}
	printf("\n");
#endif
	//////////////////////end
	if (mRetailerMode)
	{
		outBuffer[0] = 4 + 64;
		outBuffer[1] = 4 + 64;
    }
	else
	{
		outBuffer[0] = 1 + 16;
		outBuffer[1] = 1 + 16;
	}

    CRC16native_c(stringBytes, SCND_CRC_POS - MODE_BYTES, 0);

#ifdef DEBUG
    for(a=0;a<20;a++)
	{
    	printf(" %d ",(outBuffer[a]>>4)&0x0F);
    	printf(" %d ",outBuffer[a]&0x0F);
	}
	printf("\n");
#endif

	memset(fifo, -1, (TOP_INDX - BOTTOM_INDX));
	while(quads < DATABYTES*2)
	{
		//printf("Quads:%d\n", quads);
		count = 0;
		if(quads % 2 == 0)
			byteQuad = (outBuffer[quads/2] & 0xF0) >> 4;
		else
			byteQuad = outBuffer[quads/2] & 0x0F;
		for(i = 0; i < TOP_INDX - BOTTOM_INDX + 1; i++)
		{
			//printf("FIFO[%d]=%d byteQuad:%d\n", i, fifo[i], byteQuad);
			if((byteQuad != 255) && (fifo[i] == 255))
			{
				byteQuad--;
				freq = i;
			}
			else if(fifo[i] != 255)
			{
				fifo[i]--;
			}
		}

		fifo[freq] = 7;

		freq_to_send[quads++] = freq+BOTTOM_INDX;
	}
	
#ifdef DEBUG
	for (a=0;a<40;a++) {
	//		printf("%d ", freq_to_send[a]&0xff);
	}
	//printf("\n");
#endif
    //getToneCommon(freqsToSend, bytesCount * 2);

    /*FILE * file = wavfile_open("play.wav");
	if(!file) {
		printf("couldn't open sound.wav for writing:");
		return 0;
	}

	wavfile_write(file,buffer,BUF_SIZE);
	wavfile_close(file);

	system("aplay play.wav");*/

    return ;
}



