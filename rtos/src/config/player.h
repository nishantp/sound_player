/*
 * player.h
 *
 * Created: 11-Dec-17 5:10:45 PM
 *  Author: ToneTag
 */ 


#ifndef PLAYER_H_
#define PLAYER_H_
#define DATABYTES			20			//Length of data
void ToneGenerator(char *);
extern volatile unsigned char freq_to_send[DATABYTES * 2];


#endif /* PLAYER_H_ */