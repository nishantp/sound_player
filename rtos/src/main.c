/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# Minimal main function that starts with a call to system_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>
#include <player.h>
//const volatile int musicTones_sonic[]={11610-250,10717-250,9951-250,9288-250,8707-240,8195-240,7740-240,7332-220,6966-220,6634-200,6332-170,6057-160,5805-160,5572-150,5358-150,5160-140,4975-130,4804-120,4644-120,4494-120,4353-120,4221-120,4097-120,3980-120};		  //X24
//const volatile int musicTones_sonic[]={11610,10717,9951,9288,8707,8195,7740,7332,6966,6634,6332,6057,5805,5572,5358,5160,4975,4804,4644,4494,4353,4221,4097,3980};		  //X24
//const volatile int freqArray_sonic[]={16,17,18,19,20,23,26,21,35, 15, 22, 27, 18, 31, 29, 14, 26, 16, 21, 20, 27, 28, 15, 22, 12, 35, 24, 33, 17, 27, 19, 21, 26, 32, 12, 18, 13, 34, 33, 31};//123456789012345678901234567890
const volatile int freqArray_sonic[]={16,17,18,19,31,24,27,26,21,32,33,18,19,13,35,24,26,17,32,23,29,28,12,19,31,20,13,34,16,33,15,23,12,30,31,21,18,13,22,35};//D658669137
//const volatile int musicTones_sonic[]={23219-300,21433-320,19902-300,18575-300,17414-270,16390-270,15479-250,14664-260,13931-200,13267-195,12664-170,12114-180,11609-150,11144-158,10716-120,10319-185,9950-102,9607-135,9287-100,8987-90,8706-90,8443-108,8194-135,7960-100 };//48MHz
//const volatile int musicTones_sonic[]={3867,3571,3316,3095,2901,2731,2579,2443,2321,2210,2110,2018,1934,1857,1785,1719,1658,1600,1547,1497,1450,1406,1365,1326}; //8MHz
//const volatile int musicTones_sonic[]={11608.97732,10715.90215,9950.409135,9286.981859,8706.482993,8194.278111,7738.984883,7331.617257,6964.986395,6633.272757,6331.714904,6056.379474,5803.988662,5571.789116,5357.451073,5158.989922,4974.704568,4803.128548,4642.99093,4493.184771,4352.741497,4220.809936,4096.639056,3979.563654};//24MHz from sheet
const volatile int freqArray_ultra[]={211,211,211,211,211,211,211,211,224,211,219,213};//0000
const volatile int musicTones_sonic[]={23218.95465,21432.80429,19901.81827,18574.96372,17413.96599,16389.55622,15478.96977,14664.23451,13930.97279,13267.54551,12664.42981,12113.75895,11608.97732,11144.57823,10715.90215,10318.97984,9950.409135,9607.257096,9286.981859,8987.369541,8706.482993,8442.619872,8194.278111,7960.127308}; //48MHz from sheet
//const volatile int musicTones_sonic[]={23219.95465,21433.80429,19902.81827,18575.96372,17414.96599,16390.55622,15479.96977,14664.23451,13931.97279,13268.54551,12664.42981,12114.75895,11609.97732,11145.57823,10716.90215,10319.97984,9950.409135,9607.257096,9287.981859,8987.369541,8706.482993,8443.619872,8194.278111,7960.127308}; //48MHz from sheet with aprox
const volatile int musicTones_ultra[]={5307,5282,5257,5233,5208,5184,5160,5136,5113,5089,5066,5043,5021,4998,4976,4954};//96MHz
const volatile int notification_freq[]={21034,15481,17673};
xTaskHandle worker1_id;

struct tcc_module tcc_instance;
struct tcc_config config_tcc;
volatile char thisNote;


/** OSC8M calibration info */
#define FRANGE_CAL_MIN       0x00
#define FRANGE_CAL_MAX       0x03
#define TEMP_CAL_OFFSET      0x07
#define TEMP_CAL_MIN         0x00
#define TEMP_CAL_MAX         0x1F
#define COMM_CAL_MIN         0x00
#define COMM_CAL_MAX         0x7F

/** The value should be 0 or 1 */
#define CONF_FRANGE_CAL               0
#define CONF_TEMP_CAL                 1
/** The suggested value is 13 to 15 */
#define CONF_CALIBRATION_RESOLUTION   13
#define CONF_EVENT_USED_ID            EVSYS_ID_USER_TC4_EVU
#define CONF_EVENT_GENERATOR_ID       EVSYS_ID_GEN_TC3_MCX_0

/** Target OSC8M calibration frequency */
#define TARGET_FREQUENCY         8000000

/** Resolution of the calibration binary divider; lower powers of two will
 *  reduce the calibration resolution.
 */
#define CALIBRATION_RESOLUTION   CONF_CALIBRATION_RESOLUTION

/** Calibration reference clock generator index. */
#define REFERENCE_CLOCK          GCLK_GENERATOR_3

/** Frequency of the calibration reference clock in Hz */
#define REFERENCE_CLOCK_HZ       32768

/** Software instance of the USART upon which to transmit the results. */
//static struct usart_module usart_edbg;

/** Software instance of the TC module used for calibration measurement. */
static struct tc_module tc_calib;

/** Software instance of the TC module used for calibration comparison. */
static struct tc_module tc_comp;

static bool my_flag_autorize_cdc_transfert = false;
int a;
bool my_callback_cdc_enable(void)
{
	my_flag_autorize_cdc_transfert = true;
	return true;
}
void my_callback_cdc_disable(void)
{
	my_flag_autorize_cdc_transfert = false;
}
void task(void)
{
	if (my_flag_autorize_cdc_transfert)
	{
		//udi_cdc_putc('A');
		//udi_cdc_getc();
	}
}



/** Set up the measurement and comparison timers for calibration.
 *   - Configure the measurement timer to run from the CPU clock, in capture
 *     mode.
 *   - Configure the reference timer to run from the reference clock, generating
 *     a capture every time the calibration resolution count is met.
 */
static void setup_tc_channels(void)
{
	struct tc_config config;
	tc_get_config_defaults(&config);

	/* Configure measurement timer to run from Fcpu and capture */
	config.counter_size    = TC_COUNTER_SIZE_32BIT;
	config.clock_prescaler = TC_CLOCK_PRESCALER_DIV1;
	config.wave_generation = TC_WAVE_GENERATION_NORMAL_FREQ;
	config.enable_capture_on_channel[0] = true;
	tc_init(&tc_calib, TC4, &config);

	/* Configure reference timer to run from reference clock and capture when the resolution count is met */
	config.counter_size    = TC_COUNTER_SIZE_16BIT;
	config.clock_source    = REFERENCE_CLOCK;
	config.enable_capture_on_channel[0] = false;
	config.counter_16_bit.compare_capture_channel[0] = (1 << CALIBRATION_RESOLUTION);
	tc_init(&tc_comp, TC3, &config);
}

/** Set up the measurement and comparison timer events.
 *   - Configure the reference timer to generate an event upon comparison
 *     match with channel 0.
 *   - Configure the measurement timer to trigger a capture when an event is
 *     received.
 */
static void setup_tc_events(void)
{
	/* Enable incoming events on on measurement timer */
	struct tc_events events_calib = { .on_event_perform_action = true };
	tc_enable_events(&tc_calib, &events_calib);

	/* Generate events from the reference timer on channel 0 compare match */
	struct tc_events events_comp = { .generate_event_on_compare_channel[0] = true };
	tc_enable_events(&tc_comp, &events_comp);

	tc_enable(&tc_calib);
	tc_enable(&tc_comp);
}

/** Set up the event system, linking the measurement and comparison timers so
 *  that events generated from the reference timer are linked to the measurement
 *  timer.
 */
static void setup_events(struct events_resource *event)
{
	struct events_config config;

	events_get_config_defaults(&config);

	/* The event channel detects rising edges of the reference timer output
	 * event */
	config.edge_detect    = EVENTS_EDGE_DETECT_RISING;
	config.path           = EVENTS_PATH_SYNCHRONOUS;
	config.generator      = CONF_EVENT_GENERATOR_ID;

	events_allocate(event, &config);
	events_attach_user(event, CONF_EVENT_USED_ID);

}


static uint32_t get_osc_frequency(void)
{
	/* Clear any existing match status on the measurement timer */
	tc_clear_status(&tc_comp, TC_STATUS_CHANNEL_0_MATCH);

	/* Restart both measurement and reference timers */
	tc_start_counter(&tc_calib);
	tc_start_counter(&tc_comp);

	
	/* Wait for the measurement timer to signal a compare match */
	while (!(tc_get_status(&tc_comp) & TC_STATUS_CHANNEL_0_MATCH)) {
		/* Wait for channel 0 match */
	}

	/* Compute the real clock frequency from the measurement timer count and
	 * reference count */
	uint64_t tmp = tc_get_capture_value(&tc_calib, TC_COMPARE_CAPTURE_CHANNEL_0);
	return ((tmp * REFERENCE_CLOCK_HZ) >> CALIBRATION_RESOLUTION);
}


void config_tcc_fun(int period1)
{
	
	tcc_get_config_defaults(&config_tcc,TCC0);
	config_tcc.counter.reload_action=TCC_RELOAD_ACTION_RESYNC;
	config_tcc.counter.clock_source=GCLK_GENERATOR_4;
	config_tcc.counter.clock_prescaler= TCC_CLOCK_PRESCALER_DIV1;
	config_tcc.compare.wave_generation=TCC_WAVE_GENERATION_SINGLE_SLOPE_PWM;
	config_tcc.compare.wave_ramp = TCC_RAMP_RAMP1;
	config_tcc.counter.period=period1;
	config_tcc.compare.match[0]=period1/2;
	config_tcc.pins.enable_wave_out_pin[TCC_MATCH_CAPTURE_CHANNEL_0]=true;
	config_tcc.pins.wave_out_pin[TCC_MATCH_CAPTURE_CHANNEL_0]=PIN_PA08E_TCC0_WO0;
	config_tcc.pins.wave_out_pin_mux[TCC_MATCH_CAPTURE_CHANNEL_0]=MUX_PA08E_TCC0_WO0;

	tcc_init(&tcc_instance,TCC0,&config_tcc);
	tcc_enable(&tcc_instance);
}

void leds_player(void)
{
	portTickType xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();
	//port_pin_set_output_level(PIN_PA11, false);					// speaker disable
	//port_pin_set_output_level(PIN_PA11, true);					// speaker enable
	//config_tcc_fun(musicTones_ultra[15]);
	
	while(1)
	{
		port_pin_set_output_level(PIN_PA00,true);
		vTaskDelayUntil(&xLastWakeTime,4000);
		port_pin_set_output_level(PIN_PA00,false);
		vTaskDelayUntil(&xLastWakeTime,4000);
		
		port_pin_set_output_level(PIN_PA11, true);					// speaker enable
		/*for(thisNote=0;thisNote<3;thisNote++)
		{
			config_tcc_fun(notification_freq[thisNote]);
			vTaskDelayUntil(&xLastWakeTime,200);
			tcc_disable(&tcc_instance);
		}*/
		//for (thisNote = 0; thisNote < 24; thisNote++)
		for (thisNote = 0; thisNote < 16; thisNote++)
		{
			//config_tcc_fun(48000000UL/(thisNote*172.265625));
			//config_tcc_fun(musicTones_sonic[thisNote]-70);
			config_tcc_fun(musicTones_ultra[thisNote]);
			//delay_ms(30);
			//vTaskDelayUntil(&xLastWakeTime,15);		// FOR SONIC
			vTaskDelayUntil(&xLastWakeTime,10);    //  FOR ULTRA
			//config_tcc(musicTones_ultra[thisNote]);
			//delay_ms(20);
			tcc_disable(&tcc_instance);
		}
		
		//delay_ms(300);
		//vTaskDelayUntil(&xLastWakeTime,150);  // FOR SONIC
		vTaskDelayUntil(&xLastWakeTime,15);   //FOR ULTRA
		//delay_ms(10);
		//for (thisNote = 0; thisNote < 40; thisNote++)      //FOR SONIC
		for (thisNote = 0; thisNote < 12; thisNote++)     //FOR ULTRA
		{
			//config_tcc_fun(48000000UL/(freqArray_sonic[thisNote]*172.265625));
			//config_tcc_fun(musicTones_sonic[freqArray_sonic[thisNote]-12]-70);     //FOR SONIC
			//config_tcc_fun(musicTones_sonic[freq_to_send[thisNote]-12]);     //FOR SONIC
			config_tcc_fun(musicTones_ultra[freqArray_ultra[thisNote]-210]);   //FOR ULTRA
			//delay_ms(40);
			//vTaskDelayUntil(&xLastWakeTime,40);    // FOR SONIC
			vTaskDelayUntil(&xLastWakeTime,80);    //FOR ULTRA
			//config_tcc(musicTones_ultra[freqArray_ultra[thisNote]-210]);
			//delay_ms(80);
			tcc_disable(&tcc_instance);
			//vTaskDelayUntil(&xLastWakeTime,1);
		}
		port_pin_set_output_level(PIN_PA11, false);					// speaker disable
	}
}




int main (void)
{
	struct events_resource event;
	
	system_init();
	delay_init();
	struct port_config pin_conf,pin_conf_dac;
	port_get_config_defaults(&pin_conf);
	port_get_config_defaults(&pin_conf_dac);
	// Configure LEDs as outputs, turn them off 
	pin_conf.direction  = PORT_PIN_DIR_OUTPUT;
	pin_conf_dac.direction  = PORT_PIN_DIR_OUTPUT;
	pin_conf_dac.input_pull=PORT_PIN_PULL_NONE;
	port_pin_set_config(PIN_PA00, &pin_conf);                //led 0
	port_pin_set_output_level(PIN_PA00, false);					// led0 disable
	port_pin_set_config(PIN_PA01, &pin_conf);                //led 1
	port_pin_set_output_level(PIN_PA01, false);					// led1 disable
	port_pin_set_config(PIN_PA14, &pin_conf);                //oled
	port_pin_set_output_level(PIN_PA14, false);					// oled disable
	port_pin_set_config(PIN_PA01, &pin_conf);
	port_pin_set_output_level(PIN_PA01, false);
	port_pin_set_config(PIN_PA11, &pin_conf);                //speaker
	port_pin_set_output_level(PIN_PA11, true);					// speaker enable
	port_pin_set_config(PIN_PA15, &pin_conf);                 //mic
	port_pin_set_output_level(PIN_PA15, false);					//mic disable
	port_pin_set_config(PIN_PA02, &pin_conf_dac);                 //DAC&TCC
	port_pin_set_output_level(PIN_PA02, false);
	//port_
	port_pin_set_output_level(PIN_PA00,true);
	delay_ms(500);
	//vTaskDelayUntil(&xLastWakeTime,1000);
	port_pin_set_output_level(PIN_PA00,false);
	delay_ms(500);
	
	/*setup_tc_channels();
	setup_tc_events();
	setup_events(&event);
	
	port_pin_set_output_level(PIN_PA00,true);
	delay_ms(500);
	//vTaskDelayUntil(&xLastWakeTime,1000);
	port_pin_set_output_level(PIN_PA00,false);
	delay_ms(500);
	// Init the variables with default calibration settings 
	uint8_t frange_cal = SYSCTRL->OSC8M.bit.FRANGE;
	uint8_t temp_cal = SYSCTRL->OSC8M.bit.CALIB >> TEMP_CAL_OFFSET;
	uint8_t comm_cal = SYSCTRL->OSC8M.bit.CALIB & COMM_CAL_MAX;
	//Set the calibration test range 
	uint8_t frange_cal_min = max((frange_cal - CONF_FRANGE_CAL), FRANGE_CAL_MIN);
	uint8_t frange_cal_max = min((frange_cal + CONF_FRANGE_CAL), FRANGE_CAL_MAX);
	uint8_t temp_cal_min = max((temp_cal - CONF_TEMP_CAL), TEMP_CAL_MIN);
	uint8_t temp_cal_max = min((temp_cal + CONF_TEMP_CAL), TEMP_CAL_MAX);

	// Variables to track the previous and best calibration settings 
	uint16_t comm_best   = 0;
	uint8_t  frange_best = 0;
	uint32_t freq_best   = 0;
	uint32_t freq_before = get_osc_frequency();
	
	
	
	port_pin_set_output_level(PIN_PA00,true);
	delay_ms(500);
	//vTaskDelayUntil(&xLastWakeTime,1000);
	port_pin_set_output_level(PIN_PA00,false);
	delay_ms(500);
	
	
	//vTaskDelayUntil(&xLastWakeTime,1000);
	
	
	// Run calibration loop 
	for (frange_cal = frange_cal_min; frange_cal <= frange_cal_max; frange_cal++) {
		for (temp_cal = temp_cal_min; temp_cal <= temp_cal_max; temp_cal++) {
			for (comm_cal = COMM_CAL_MIN; comm_cal <= COMM_CAL_MAX; comm_cal++) {
				//Set the test calibration values 
				system_clock_source_write_calibration(
						SYSTEM_CLOCK_SOURCE_OSC8M, (temp_cal << 7) | comm_cal, frange_cal);

				// Wait for stabilization 
				delay_cycles(1000);

				// Compute the deltas of the current and best system clock
			//	 * frequencies, save current settings if they are closer to the
			//	 * ideal frequency than the previous best values
				 
				uint32_t freq_current = get_osc_frequency();
				if (abs(freq_current - TARGET_FREQUENCY) < abs(freq_best - TARGET_FREQUENCY)) {
					freq_best   = freq_current;
					comm_best   = comm_cal;
					frange_best = frange_cal;

					port_pin_set_output_level(PIN_PA00, false);
				} else {
					port_pin_set_output_level(PIN_PA00, !false);
				}
			}
		}
	}
	
	
	// Set the found best calibration values 
	system_clock_source_write_calibration(
	SYSTEM_CLOCK_SOURCE_OSC8M, (temp_cal << 7) | comm_best, frange_best);*/
	port_pin_set_output_level(PIN_PA11, false);					// speaker disable
	port_pin_set_output_level(PIN_PA00,true);
	delay_ms(500);
	//vTaskDelayUntil(&xLastWakeTime,1000);
	port_pin_set_output_level(PIN_PA00,false);
	delay_ms(500);
	port_pin_set_output_level(PIN_PA00,true);
	delay_ms(500);
	//vTaskDelayUntil(&xLastWakeTime,1000);
	port_pin_set_output_level(PIN_PA00,false);
	delay_ms(500);
	
	//udc_start();
	//stdio_usb_init();
	//stdio_usb_enable();
	//ToneGenerator("D658669137");
	
	xTaskCreate(leds_player,"Worker 1",configMINIMAL_STACK_SIZE,NULL,1,&worker1_id);
	vTaskStartScheduler();
	
	while(1)
	{
		
	}
	/* Insert application code here, after the board has been initialized. */
}
